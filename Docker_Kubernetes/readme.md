# Docker and Kubernetes

This project involves creating a web server application using Python Flask, containerizing it with Docker, and deploying it on Kubernetes.


## Requirements:

1. Create a small web server app that displays "Hello World!". This could be done in any language you like, but Python is recommended. You can use other modules/ways to do it, but we recommend using Flask. The browser must be pointed to http://localhost.
2. Dockerize it: package the newly created app into a Docker container. You should deliver a Dockerfile and a docker command to run it.
3. Create a Kubernetes deployment or pod to be able to run in Kubernetes. Create a YAML file containing the right Kubernetes pod/deployment that would allow this application to run in Kubernetes.



## Prerequisites on your local machine:
- docker desktop
- python flask
- Kubernetes Cluster dev environment

## How to simulate this

**1. Create the container**

```
cd container
docker build -t helloworld .
```

**2. Run the docker image locally to see if it's working as expected**

```
docker run -p 5000:5000 helloworld
```

Open a browser and navigate to http://localhost:5000. You should see the message "Hello, World!".

**3. Deploy to Kubernetes**

    There are two deployment file
        - `hello_world.yml` is the deployment file for deploying the application
        - `service.yml` is a deployment of a service that will expose the app to outside world

```
kubectl apply -f deployment.yml
kubectl apply -f service.yml
```

**4. Wait for deployment to be completed and test it**

```
kubectl describe pod helloworld-deployment-****
```


## References

### What apps would you use and why?**
[Python Flask](https://readthedocs.org/projects/flask/) is a ==lightweight web framework== that is used for building web applications with Python

### Which metrics would you monitor?

- **Resources** utilization such as CPU and memory - it is important to identify if your application is over or underutilizing resources and monitor if there are any bottlenecks on performance.
- **Network** latency to see if impacts your application's performance
- **Load balancing** to ensure that your application is being distributed evenly accross nodes and no performance issues.
- **Kubernetes metrics**, to see if the performance of the infra is impacting your application.

### How would you create a CI/CD pipeline for this application? (Please, give a Gitlab,Travis, Jenkins, Github, etc. example)

Below is how I would do it in Gitlab, if fingerprinting is possible, fingerprint the artifacts for different environment and make sure it is consistent when deploying to higher environments.

```
stages:
  - build
  - test
  - deploy

build:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  script:
    - all the step
    - to create the container
    - and push it to registry

fingerprint:
  stage: build
  image:
  script:
    - command to connect to any tools available to fingerprint the artifact.

test:
  stage: test
  script:
    - docker run --rm <your-app-container-from build stage>

deploy:
  stage: deploy
  script:
    - kubectl commands to deploy your app to kubernetes
```

### How would you make sure the app has the right resources? (Limits and resources in the Kubernetes deployment).

You can set it up on your deployment manifest file, below is an example.

```
    containers:
    - name: my-app
      image: my-app:v1
      resources:
        requests:
          cpu: 500m
          memory: 512Mi
        limits:
          cpu: 1000m
          memory: 1024Mi
```

In this example, the requests field specifies that the app needs at least 500 milli CPU and 512 mebibytes of memory to run. The limits field specifies that the app can use up to 1000 milli CPU and 1024 mebibytes of memory. These values can be adjusted based on the specific requirements of your app.

### Files
```
Docker_Kubernetes
├── container
│   ├── Dockerfile
│   ├── hello_world.py
│   └── requirements.txt
├── hello_world.yml
├── readme.md
└── service.yml
```
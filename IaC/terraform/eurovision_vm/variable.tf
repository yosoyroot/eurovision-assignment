variable "region" {
  type    = string
  default = "eu-north-1"
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "private_ips" {
  type    = list(string)
}

variable "ami" {
  type    = string
}

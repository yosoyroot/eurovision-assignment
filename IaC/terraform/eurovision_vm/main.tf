terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 1.4.5"
}


#https://docs.localstack.cloud/user-guide/integrations/terraform/

provider "aws" {
  access_key                  = "test"
  secret_key                  = "test"
  region                      = var.region
  s3_force_path_style         = false
  skip_credentials_validation = true
  skip_metadata_api_check     = true
  skip_requesting_account_id  = true

  endpoints {
    apigateway     = "http://localhost:4566"
    apigatewayv2   = "http://localhost:4566"
    cloudformation = "http://localhost:4566"
    cloudwatch     = "http://localhost:4566"
    dynamodb       = "http://localhost:4566"
    ec2            = "http://localhost:4566"
    es             = "http://localhost:4566"
    elasticache    = "http://localhost:4566"
    firehose       = "http://localhost:4566"
    iam            = "http://localhost:4566"
    kinesis        = "http://localhost:4566"
    lambda         = "http://localhost:4566"
    rds            = "http://localhost:4566"
    redshift       = "http://localhost:4566"
    route53        = "http://localhost:4566"
    s3             = "http://s3.localhost.localstack.cloud:4566"
    secretsmanager = "http://localhost:4566"
    ses            = "http://localhost:4566"
    sns            = "http://localhost:4566"
    sqs            = "http://localhost:4566"
    ssm            = "http://localhost:4566"
    stepfunctions  = "http://localhost:4566"
    sts            = "http://localhost:4566"
  }
}

# Create VPC
resource "aws_vpc" "eurovision_vpc" {
  cidr_block = "10.0.0.0/24"
}

# Create subnet in VPC
resource "aws_subnet" "eurovision" {
  vpc_id     = aws_vpc.eurovision_vpc.id
  cidr_block = "10.0.0.0/28"
}

# Create subnet in Network interface

resource "aws_eip" "eurovision" {
  vpc = true
}

resource "aws_network_interface" "eurovision" {
  subnet_id       = aws_subnet.eurovision.id
  private_ips     = var.private_ips
}

resource "aws_security_group" "eurovision" {
  name_prefix = "eurovision"
}

resource "aws_security_group_rule" "eurovision_ingress" {
  type        = "ingress"
  from_port   = 0
  to_port     = 65535
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.eurovision.id
}

resource "aws_network_interface_sg_attachment" "eurovision" {
  security_group_id    = aws_security_group.eurovision.id
  network_interface_id = aws_network_interface.eurovision.id
}

resource "aws_eip_association" "eurovision" {
  network_interface_id = aws_network_interface.eurovision.id
  allocation_id        = aws_eip.eurovision.id
}

# Launch EC2 instance
resource "aws_instance" "Test-eurovision" {
  ami           = var.ami # Centos 7 AMI
  instance_type = var.instance_type
  network_interface {
    network_interface_id = aws_network_interface.eurovision.id
    device_index         = 0
  }
  tags = {
    Name = "Test-eurovision"
  }
}
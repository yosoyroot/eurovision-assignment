# Infrastructure as a Code

The objective on this section is to deploy AWS infrastructure with Terraform and test it with LocalStack. The deployment must follow the following diagram:

## Requirements

**The infrastructure to deploy must include:**

- A VPC with an IP addressing of 10.0.0.0/24.
- A Subnet with an IP addressing of 10.0.0.0/28.
- An EC2 instance with the information shown in the image.
- A network interface attached to the EC2 with the information shown in the image.

**The folder to deliver must contain the following:**

1. Variable files with all the variables of the infrastructure. 
2. Main file including the required provider.
3. The credentials file with the mock credentials.
4. The file to describe the network and the VM.

## Prerequisites on your local machine:

- [localstack] (https://localstack.cloud/)
- terraform

## How does this work

The `main.tf` will execute as follows:

- Create the VPC
- Create a Subnet and associate it to the newly created VPC
- Create amd setup a network interface
    - Elastic IP address
    - Security group rule
- Launch an EC2 instance


## How to simulate this

- Go to terraform directory and run below commands

```
terraform plan -var-file eurovision.tfvars
terraform apply -var-file eurovision.tfvars
```

- Verify your work

```
aws --endpoint-url=http://localhost:4566 ec2 describe-instances
```

## TODO

- Clean it up a bit more
- Create a pipeline
- Usecase for different environment





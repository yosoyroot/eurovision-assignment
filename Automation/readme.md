# AUTOMATION

This project demonstrates the use of Ansible for automation, specifically for installing Terraform-Local using pip on a Docker container.

### Requirements:

1. Hosts file (even if the tasks will be only performed on the same container)
2. One role: you’ll need to create one ansible role named ‘bootstrap’.
3. Ansible playbook that will trigger the execution of the role


## Prerequisites on your local machine:
- docker desktop
- ansible

## How to simulate this

By executing the `run.sh` script, you can initiate a playbook `deploy_ubuntu.yml` that deploys an Ubuntu container on Docker Desktop.

The playbook `deploy_ubuntu.yml` contains 3 main tasks

- Deploy the Ubuntu container
- Setup us the container environment with necessary packages and settings
- Setup the user and running the exercise as that user.
    
    The script user_setup.sh includes below:
     
        - creating necessary folders
        - setting up python environment
        - creating necessary ansible files as per requirement
        - runs the playbook that install terraform-local on the container


### Files


```
├── Automation
│   ├── ansible.cfg
│   ├── deploy_ubuntu.yml
│   ├── env_setup.sh
│   ├── readme.md
│   ├── rollback.sh
│   ├── run.sh
│   └── user_setup.sh
```

### TODO
- Clean up the scripts and make it interactive.
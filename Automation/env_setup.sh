#!bin/bash

echo "Updating apt and installing ansible"
apt update

# getting issue with installation of multiple packages where it hangs so the next task gets error
# Waiting for cache lock: Could not get lock /var/lib/dpkg/lock-frontend. It is held by process 215 (apt)..
#apt install ansible python3-pip python3-venv -y

# looping the installation instead
packages=(ansible python3-pip python3-venv)

for pkg in "${packages[@]}"
do
  apt-get install $pkg -y
done

export SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt



echo "Add local user"
useradd wendy  -s /bin/bash -d /home/wendy
mkdir -p /home/wendy 
chown 1000:1000 /home/wendy


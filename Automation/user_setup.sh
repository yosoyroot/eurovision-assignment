export SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt

mkdir -p ~/ansible/roles/bootstrap/tasks
mkdir -p ~/ansible/inventory
touch ~/ansible/hosts ~/ansible/bootstrap.yml ~/ansible/roles/bootstrap/tasks/main.yml

python3 -m ~/my-project-env
source ~/my-project-env/bin/activate


cat <<EOF > ~/ansible/inventory/hosts
---
hosts:
  localhost:
   vars:
     ansible_connection: local
     ansible_python_interpreter: "{{ansible_playbook_python}}"
EOF

cat ~/ansible/inventory/hosts

echo "Creating the bootstrap role task"

cat <<EOF > ~/ansible/roles/bootstrap/tasks/main.yml
---
- name: Install Terraform via pip
  ansible.builtin.pip:
    name: terraform-local
    state: latest
EOF

cat ~/ansible/roles/bootstrap/tasks/main.yml

echo "Creating the playbook"

cat <<EOF > ~/ansible/bootstrap.yml 
---
- hosts: localhost
  roles:
    - bootstrap
EOF



cat ~/ansible/bootstrap.yml 

echo "Running the playbook"

ansible-playbook -i ~/ansible/inventory  ~/ansible/bootstrap.yml -vvv


echo "Checking if terraform-local was installed"

pip list |grep terraform